# CxApp

Requirements: 
- CxServer up-and-running instance, network-accessible from Gitlab instance 
- Environment variables:
    - CX_USERNAME
    - CX_PASSWORD (if it has a $ make sure you input two $$)
    - CX_SERVER
    - CX_SERVER_URI
    - CXARM_SERVER_URI (needed to break build on a violated policy)
    - GL_COMMENT_TOKEN (created by User Account -> Settings -> Access Tokens)
    - GL_URL (i.e. https://gitlab.com)